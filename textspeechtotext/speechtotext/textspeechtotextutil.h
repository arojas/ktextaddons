/*
  SPDX-FileCopyrightText: 2023 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <QString>
namespace TextSpeechToText
{
namespace TextSpeechToTextUtil
{
[[nodiscard]] QString groupTranslateName();
[[nodiscard]] QString engineTextToSpeechName();
[[nodiscard]] QString defaultEngineName();
};
}

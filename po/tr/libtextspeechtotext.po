# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the ktextaddons package.
#
# Emir SARI <emir_sari@icloud.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: ktextaddons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:28+0000\n"
"PO-Revision-Date: 2023-10-24 12:26+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.11.70\n"

#: speechtotext/plugins/google/googlespeechtotextclient.cpp:24
#, kde-format
msgid "Google"
msgstr "Google"

#: speechtotext/plugins/vosk/generateinstalledlanguageinfojob.cpp:23
#, kde-format
msgid "Impossible to store language info."
msgstr "Dil bilgisini depolamak olanaksız"

#: speechtotext/plugins/vosk/managermodelvoskspeechtotext.cpp:49
#: speechtotext/plugins/vosk/voskdownloadlanguagejob.cpp:49
#, kde-format
msgid ""
"Error: Engine systems have detected suspicious traffic from your computer "
"network. Please try your request again later."
msgstr ""
"Hata: İşletke sistemleri, bilgisayar ağınızdan kuşku uyandırıcı trafik "
"algıladı. Lütfen, isteğinizi daha sonra yineleyin."

#: speechtotext/plugins/vosk/managermodelvoskspeechtotext.cpp:51
#: speechtotext/plugins/vosk/voskdownloadlanguagejob.cpp:51
#, kde-format
msgid "Impossible to access to url: %1"
msgstr "URL'ye erişim olanaksız: %1"

#: speechtotext/plugins/vosk/voskdownloadlanguagejob.cpp:37
#, kde-format
msgid "Cannot open file for downloading."
msgstr "Dosya indirmek için açılamıyor."

#: speechtotext/plugins/vosk/voskdownloadlanguagejob.cpp:62
#, kde-format
msgid "CheckSum is not correct."
msgstr "Sağlama toplamı doğru değil."

#: speechtotext/plugins/vosk/voskdownloadlanguagejob.cpp:72
#, kde-format
msgid "Error during writing on disk: %1"
msgstr "Diske yazarken hata: %1"

#: speechtotext/plugins/vosk/voskenginedialog.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Vosk Plugin Settings"
msgstr "Vosk Eklentisi Ayarları"

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:52
#, kde-format
msgid "Search..."
msgstr "Ara..."

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:90
#, kde-format
msgid "Download"
msgstr "İndir"

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:111
#, kde-format
msgid "Delete"
msgstr "Sil"

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:115
#, kde-format
msgid "Update List"
msgstr "Listeyi Güncelle"

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:116
#, kde-format
msgid "Update list of languages from network"
msgstr "Ağdan dil listesini güncelle"

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:192
#, kde-format
msgid "Error: %1"
msgstr "Hata: %1"

#: speechtotext/plugins/vosk/voskenginelanguagewidget.cpp:192
#: speechtotext/plugins/vosk/voskspeechtotextclient.cpp:26
#, kde-format
msgid "Vosk"
msgstr "Vosk"

#: speechtotext/plugins/vosk/voskextractlanguagejob.cpp:56
#, kde-format
msgid "Impossible to extract language"
msgstr "Dili çıkarmak olanaksız"

#: speechtotext/plugins/vosk/voskspeechtotextmodel.cpp:40
#, kde-format
msgid "Language"
msgstr "Dil"

#: speechtotext/plugins/vosk/voskspeechtotextmodel.cpp:42
#: speechtotext/plugins/vosk/voskspeechtotextmodel.cpp:85
#, kde-format
msgid "Installed"
msgstr "Kurulu"

#: speechtotext/plugins/vosk/voskspeechtotextmodel.cpp:44
#, kde-format
msgid "Installed Version"
msgstr "Kurulu Sürüm"

#: speechtotext/plugins/vosk/voskspeechtotextmodel.cpp:46
#, kde-format
msgid "Available Version"
msgstr "Kullanılabilir Sürüm"

#: speechtotext/plugins/vosk/voskspeechtotextmodel.cpp:48
#, kde-format
msgid "Size"
msgstr "Boyut"

#: speechtotext/plugins/whisper/whisperspeechtotextclient.cpp:24
#, kde-format
msgid "Whisper"
msgstr "Whisper"

#: speechtotext/widgets/speechtotextcomboboxwidget.cpp:28
#, kde-format
msgid "Engine:"
msgstr "İşletke:"

#: speechtotext/widgets/speechtotextconfiguredialog.cpp:18
#, kde-format
msgctxt "@title:window"
msgid "Configure Translator"
msgstr "Çevirmeni Yapılandır"

#: speechtotext/widgets/speechtotextmenu.cpp:17
#, kde-format
msgid "Speech to Text..."
msgstr "Konuşmadan Metne..."

#: speechtotext/widgets/speechtotextselectdevicewidget.cpp:31
#, kde-format
msgid "Input:"
msgstr "Girdi:"

#: speechtotext/widgets/speechtotextselectdevicewidget.cpp:70
#, kde-format
msgid "Default"
msgstr "Öntanımlı"

#~ msgid "Obsolete"
#~ msgstr "Eski"

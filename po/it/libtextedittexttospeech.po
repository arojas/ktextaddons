# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the ruqola package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: ruqola\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:28+0000\n"
"PO-Revision-Date: 2023-01-15 13:27+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.1\n"

#: texttospeechactions.cpp:28 texttospeechactions.cpp:31
#, kde-format
msgid "Stop"
msgstr "Ferma"

#: texttospeechactions.cpp:72
#, kde-format
msgid "Pause"
msgstr "Pausa"

#: texttospeechactions.cpp:72
#, kde-format
msgid "Play"
msgstr "Riproduci"

#: texttospeechconfigdialog.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "Configure Text-To-Speech"
msgstr "Configura sintesi vocale"

#: texttospeechconfiginterface.cpp:88
#, kde-format
msgid "Morning, this is the test for testing settings."
msgstr "Buongiorno, questo è il test per provare le impostazioni."

#: texttospeechconfigwidget.cpp:34
#, kde-format
msgid "Test"
msgstr "Prova"

#: texttospeechconfigwidget.cpp:41 texttospeechwidget.cpp:59
#, kde-format
msgid "Volume:"
msgstr "Volume:"

#: texttospeechconfigwidget.cpp:45
#, kde-format
msgid "Rate:"
msgstr "Velocità:"

#: texttospeechconfigwidget.cpp:51
#, kde-format
msgid "Pitch:"
msgstr "Tono:"

#: texttospeechconfigwidget.cpp:54
#, kde-format
msgid "Engine:"
msgstr "Motore:"

#: texttospeechconfigwidget.cpp:58
#, kde-format
msgid "Language:"
msgstr "Lingua:"

#: texttospeechconfigwidget.cpp:62
#, kde-format
msgid "Voice:"
msgstr "Voce:"

#: texttospeechwidget.cpp:54
#, kde-format
msgid "Close"
msgstr "Chiudi"

#: texttospeechwidget.cpp:80
#, kde-format
msgid "Configure..."
msgstr "Configura..."

#: texttospeechwidget.cpp:132
#, kde-format
msgid "Engine has a problem."
msgstr "Il motore ha un problema."

#: texttospeechwidget.cpp:132
#, kde-format
msgid "Text To Speech"
msgstr "Sintesi vocale"

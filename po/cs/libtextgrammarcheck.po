# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdepim-addons package.
# Vit Pelcak <vit@pelcak.org>, 2019, 2020, 2021, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kdepim-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:28+0000\n"
"PO-Revision-Date: 2023-01-24 15:34+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.1\n"

#: common/grammarresulttextedit.cpp:39
#, kde-format
msgid "Any text to check."
msgstr ""

#: common/grammarresulttextedit.cpp:93
#, kde-format
msgid "Replacement"
msgstr "Nahrazení"

#: common/grammarresulttextedit.cpp:102
#, kde-format
msgid "Online Grammar Information"
msgstr ""

#: common/grammarresulttextedit.cpp:115
#, kde-format
msgid "Check Again"
msgstr "Znovu zkontrolovat"

#: common/grammarresulttextedit.cpp:118
#, kde-format
msgid "Configure..."
msgstr "Nastavit..."

#: common/grammarresultutil.cpp:49
#, kde-format
msgid "See on: %1"
msgstr ""

#: common/grammarresultwidget.cpp:32
#, kde-format
msgid "Close"
msgstr "Zavřít"

#: grammalecte/grammalecteconfigdialog.cpp:25
#, kde-format
msgctxt "@title:window"
msgid "Configure Grammalecte"
msgstr "Nastavit Grammalecte"

#: grammalecte/grammalecteconfigwidget.cpp:35
#, kde-format
msgid "General"
msgstr "Obecné"

#: grammalecte/grammalecteconfigwidget.cpp:36
#, kde-format
msgid "Grammar Settings"
msgstr "Nastavení mluvnice"

#: grammalecte/grammalecteconfigwidget.cpp:61
#, kde-format
msgid ""
"Impossible to get options. Please verify that you have grammalected "
"installed."
msgstr ""

#: grammalecte/grammalecteconfigwidget.cpp:62
#, kde-format
msgid "Error during Extracting Options"
msgstr ""

#: grammalecte/grammalecteconfigwidget.cpp:111
#, kde-format
msgid "Press Button for Reloading Settings"
msgstr ""

#: grammalecte/grammalecteconfigwidget.cpp:118
#, kde-format
msgid "Reload Settings"
msgstr "Obnovit nastavení"

#: grammalecte/grammalecteconfigwidget.cpp:135
#, kde-format
msgid "Python Path:"
msgstr ""

#: grammalecte/grammalecteconfigwidget.cpp:139
#, kde-format
msgid "Add full 'grammalecte-cli.py' path"
msgstr ""

#: grammalecte/grammalecteconfigwidget.cpp:141
#, kde-format
msgid "Grammalecte Path:"
msgstr "Cesta k Grammalecte:"

#: grammalecte/grammalecteconfigwidget.cpp:147
#, kde-format
msgid ""
"Grammalecte can be found <a href=\"https://grammalecte.net/#download\">here</"
"a>. Download it and extract it."
msgstr ""

#: grammalecte/grammalecteresultwidget.cpp:54
#, kde-format
msgid "Python path is missing."
msgstr "Cesta k Pythonu chybí."

#: grammalecte/grammalecteresultwidget.cpp:58
#, kde-format
msgid "Grammalecte path not found."
msgstr "Cesta ke Grammalecte nenalezena."

#: grammalecte/grammalecteresultwidget.cpp:65
#, kde-format
msgid "Grammalecte program file not found."
msgstr "Soubor programu Grammalecte nenalezen."

#: grammalecte/grammalecteresultwidget.cpp:69
#, kde-format
msgid "Grammalecte cli file not found."
msgstr ""

# error box title
#: grammalecte/grammalecteresultwidget.cpp:73
#, kde-format
msgid "Grammalecte error"
msgstr "Chyba Grammalecte"

#: grammalecte/grammalecteurlrequesterwidget.cpp:37
#, kde-format
msgid "Select Path"
msgstr "Vyberte cestu"

#: languagetool/languagetoolcombobox.cpp:31
#, kde-format
msgid "English"
msgstr "Anglický"

#: languagetool/languagetoolcombobox.cpp:32
#, kde-format
msgid "Asturian"
msgstr "Asturian"

#: languagetool/languagetoolcombobox.cpp:33
#, kde-format
msgid "Belarusian"
msgstr "Běloruský"

#: languagetool/languagetoolcombobox.cpp:34
#, kde-format
msgid "Breton"
msgstr "Bretonština"

#: languagetool/languagetoolcombobox.cpp:35
#, kde-format
msgid "Catalan"
msgstr "Katalánský"

#: languagetool/languagetoolcombobox.cpp:36
#, kde-format
msgid "Chinese"
msgstr "Čínština"

#: languagetool/languagetoolcombobox.cpp:37
#, kde-format
msgid "Danish"
msgstr "Dánský"

#: languagetool/languagetoolcombobox.cpp:38
#, kde-format
msgid "Dutch"
msgstr "Nizozemský"

#: languagetool/languagetoolcombobox.cpp:39
#, kde-format
msgid "English (Australian)"
msgstr ""

#: languagetool/languagetoolcombobox.cpp:40
#, kde-format
msgid "English (Canadian)"
msgstr ""

#: languagetool/languagetoolcombobox.cpp:41
#, kde-format
msgid "Esperanto"
msgstr "Esperanto"

#: languagetool/languagetoolcombobox.cpp:42
#, kde-format
msgid "French"
msgstr "Francouzština"

#: languagetool/languagetoolcombobox.cpp:43
#, kde-format
msgid "Galician"
msgstr "Galicijský"

#: languagetool/languagetoolcombobox.cpp:44
#, kde-format
msgid "German"
msgstr "Němčina"

#: languagetool/languagetoolcombobox.cpp:45
#, kde-format
msgid "Greek"
msgstr "Řecký"

#: languagetool/languagetoolcombobox.cpp:46
#, kde-format
msgid "Italian"
msgstr "Italština"

#: languagetool/languagetoolcombobox.cpp:47
#, kde-format
msgid "Japanese"
msgstr "Japonština"

#: languagetool/languagetoolcombobox.cpp:48
#, kde-format
msgid "Khmer"
msgstr "Khmérské"

#: languagetool/languagetoolcombobox.cpp:49
#, kde-format
msgid "Persian"
msgstr "Perský"

#: languagetool/languagetoolcombobox.cpp:50
#, kde-format
msgid "Polish"
msgstr "Polský"

#: languagetool/languagetoolcombobox.cpp:51
#, kde-format
msgid "Portuguese"
msgstr "Portugalština"

#: languagetool/languagetoolcombobox.cpp:52
#, kde-format
msgid "Romanian"
msgstr "Rumunský"

#: languagetool/languagetoolcombobox.cpp:53
#, kde-format
msgid "Russian"
msgstr "Ruština"

#: languagetool/languagetoolcombobox.cpp:54
#, kde-format
msgid "Slovak"
msgstr "Slovenský"

#: languagetool/languagetoolcombobox.cpp:55
#, kde-format
msgid "Slovenian"
msgstr "Slovinský"

#: languagetool/languagetoolcombobox.cpp:56
#, kde-format
msgid "Spanish"
msgstr "Španělština"

#: languagetool/languagetoolcombobox.cpp:57
#, kde-format
msgid "Swedish"
msgstr "Švédský"

#: languagetool/languagetoolcombobox.cpp:58
#, kde-format
msgid "Tagalog"
msgstr "Tagalog"

#: languagetool/languagetoolcombobox.cpp:59
#, kde-format
msgid "Tamil"
msgstr "Tamilština"

#: languagetool/languagetoolcombobox.cpp:60
#, kde-format
msgid "Ukrainian"
msgstr "Ukrajinský"

#: languagetool/languagetoolconfigdialog.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Configure LanguageTool"
msgstr "Nastavit Jazykový nástroj"

#: languagetool/languagetoolconfigwidget.cpp:25
#, kde-format
msgid "Use Local Instance"
msgstr ""

#: languagetool/languagetoolconfigwidget.cpp:27
#, kde-format
msgid "Instance Path:"
msgstr ""

#: languagetool/languagetoolconfigwidget.cpp:58
#, kde-format
msgid "Language:"
msgstr "Jazyk:"

#: languagetool/languagetoolconfigwidget.cpp:70
#, kde-format
msgid "Refresh"
msgstr "Obnovit"

#: languagetool/languagetoolresultwidget.cpp:60
#, kde-format
msgid "An error was reported: %1"
msgstr ""

#: languagetool/languagetoolresultwidget.cpp:60
#, kde-format
msgid "Failed to check grammar."
msgstr ""

#: languagetool/languagetoolresultwidget.cpp:76
#, kde-format
msgid "Check"
msgstr "Zkontrolovat"

#: languagetool/languagetoolupdatecombobox.cpp:75
#, kde-format
msgid ""
"An error occurred attempting to load the list of available languages:\n"
"%1"
msgstr ""

#: languagetool/languagetoolupdatecombobox.cpp:75
#, kde-format
msgid "List of Languages"
msgstr ""
